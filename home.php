<?php 

$path = "home";
session_start();

		
		// if(!isset($_SESSION['uid'])){
		// 	header("Location: ./login.php");
		// 	die();
		// }

include './header.php';
include './db.con.php';






 ?>

<div class="banner" id="banner">
	<div class="banner-text">
		<h2>Welcome To Voyage <?php 
		if(isset($_SESSION['uid'])){
			echo $_COOKIE['fname']; 
		}else{
			echo "";
		}
		?></h2>
	</div>
	
</div>

<div class="service">
	<h1>Services</h1>

	<div class="icons">


		<a href="#transport"><img src="./icons/map-pin.svg" class="service-icons">
			<h2>Transportion</h2>
		</a>


		<a href="#care"><img src="./icons/call-center-agent.svg" class="service-icons">
			<h2>Best service</h2>

		</a>

		<a href="#deals"><img src="./icons/price-tags.svg" class="service-icons">
			<h2>Best Price Guarantee</h2>

		</a>


		<a href="#rest"><img src="./icons/cutlery.svg" class="service-icons">
			<h2>Restaurant</h2>

		</a>

		<a href="#hotel"><img src="./icons/building.svg" class="service-icons">
			<h2>Handpicked Hotels</h2>

		</a>

	</div>

	<div class="service-pics">


	<div class="ser-picl">
		<div class="tradeal">


		<div id="transport" class="transport samediv">
			
		</div>

		<div id="deals" class="deals samediv">
			
		</div>



		</div>


		<div id="care" class="care">
			
		</div>

		
	</div>



	<div class="ser-picr">
		<div id="rest" class="rest samediv"></div>

		<div  id="hotel" class="hotel samediv"></div>

		
	</div>


	</div>
	
	
</div>



<!-- rooms -->
<div class="room">

	<div class="roomdiv room-pic"></div>
	<div class="roomdiv room-info">
			
			<h1 class="room-title">Premium Rooms</h1>
			<h3><span class="room-price">400$</span>/ Day</h3>
			<h3><span>Size</span>: 30 ft</h3>
			<h3><span>Capacity</span>: Max Person 5 </h3>
			<h3><span>Bed</span>: King Beds </h3>
			<h3><span> Services</span>: Wifi,Television,Bathroom</h3>


			<button class="viewd"><a href="./rooms.php">MORE ROOMS</a></button>



	</div>
	
</div>



<div class="package">
	<div>
		<h1>Packages</h1>
	</div>

	<div class="tours">

<!-- display Tours Start -->

	<?php 

	if($con){

		$query = "SELECT * FROM tours;";

		$result = mysqli_query($con,$query);

		if(mysqli_num_rows($result)>0){

			while($row = mysqli_fetch_assoc($result)){
			echo '<div class="tour-item"><a href="./tour_details.php?t_id='.$row['tid'].'">
							<img class="tour-img" src="'.$row['thumbnail'].'">
				
							<h2>'.$row['location'].' Tour</h2>
							<div class="rating">
							<img  src="./icons/calendar.svg">
							<small>'.$row['days'].' DAYS '.$row['nights'].' NIGHTS </small>
							
							</div>
							
							<div class="tour-book">
								<!-- <h3>1000$</h3> -->
								<img class="stars" src="./icons/rating.svg"> 
								<button> <span class="triangle-right "></span> Book Now </button>
							</div>
							</a>
						</div>';
			}



		}else{
			echo "Result ERROR";
		}	
		
	}else{
		echo "CON ERROR";
	}


	
	 ?>
<!-- display Tours end -->

		
		
	</div>

	
</div>



	
<?php 
include './footer.php';

 ?>