-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2021 at 08:15 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `voyage`
--

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE `tours` (
  `tid` int(11) NOT NULL,
  `location` varchar(40) NOT NULL,
  `days` int(2) NOT NULL,
  `nights` int(2) NOT NULL,
  `thumbnail` varchar(200) NOT NULL,
  `tour_des` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`tid`, `location`, `days`, `nights`, `thumbnail`, `tour_des`) VALUES
(1, 'Greece', 5, 4, './uploads/greece.jpg', 'General Greece is a country full of myths, traditions, history and a vibrant culture. By choosing the classic tour of Greece, get ready to get to know this magical place and be enchanted by its unique natural beauty and its rich historical heritage.'),
(2, 'Maldives', 4, 5, './uploads/maldives.jpg', 'For many centuries the Maldivian economy was entirely dependent on fishing and other marine products. Fishing remains the main occupation of the people and the government gives priority to the fisheries sector.'),
(3, 'Switzerland', 3, 2, './uploads/switzerland.jpg', 'Switzerland is a beautiful, tourist-attracting country. While it is most famous for its ski resorts during the winter, it is beautiful all year round. ... The Alpine mountains stretch across the Balkans to France, making Switzerland the perfect skiing destination'),
(4, 'Japan', 3, 3, './uploads/japan.jpg', 'Japan has 21 World Heritage Sites, including Himeji Castle, Historic Monuments of Ancient Kyoto and Nara. Popular foreigner attractions include Tokyo and Hiroshima, Mount Fuji, ski resorts such as Niseko in Hokkaido, Okinawa'),
(5, 'Italy', 4, 5, './uploads/italy.jpg', 'Italy for its rich culture, cuisine, history, fashion and art, its beautiful coastline and beaches, its mountains, and priceless ancient monuments. Italy also contains more World Heritage Sites than any other country in the world'),
(6, 'Thailand', 4, 5, './uploads/thailand.jpg', 'Thailand is called the Land of Smiles by those who visit? But did you know that Thailand is made up of 1,430 islands, is a Buddist Nation, and its original name was Siam'),
(7, 'Singapore', 4, 3, './uploads/singapore.jpg', 'Singapore is known for its glitz and glamour. Often described as one of the cleanest and most orderly cities in the world, the island city-state is also undoubtedly one of the world\'s most popular visitor destinations.'),
(8, 'Dubai', 5, 6, './uploads/dubai.jpg', 'The city of gold AKA Dubai is a truly international country and one of the seven Emirates in UAE. Dubai is among the handful of countries in Asia that have developed in line with western technology but retained their Eastern sensibility.'),
(9, 'Europe', 6, 7, './uploads/europe.jpg', 'The second smallest continent in the world, Europe covers 2% of the Earth’s surfaceThe second smallest continent in the world, Europe covers 2% of the Earth’s surface, Europe is also home to one of the seven wonders of the world, The Colosseum in Italy.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`tid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
